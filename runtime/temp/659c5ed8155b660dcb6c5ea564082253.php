<?php /*a:3:{s:57:"D:\phpstudy_pro\lab\application/home/view\index\home.html";i:1618225480;s:64:"D:\phpstudy_pro\lab\application/home/view\index\base\header.html";i:1612501231;s:64:"D:\phpstudy_pro\lab\application/home/view\index\base\footer.html";i:1612008989;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<!--    <link href="/static/home/css/demo/webuploader-demo.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="http://fex.baidu.com/webuploader/css/demo.css">
<!--    <link href="https://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="/static/home/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>

<title>实验室管理系统</title>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close"><i class="fa fa-times-circle"></i></div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span><img alt="image" class="img-circle" src="/static/home/icon.jpg" width="70" height="70"/></span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?php echo htmlentities($user['name']); ?>
                                        <br>
                                        <small>
                                            <?php switch($user['authority']): case "4": ?>(系统管理员)<?php break; case "3": ?>(资产管理员)<?php break; case "2": ?>(实验室主任)<?php break; case "1": ?>(实验员)<?php break; default: ?>(授权用户)
                                            <?php endswitch; ?>
                                        </small>
                                    </strong>
                                </span>
                                <span class="text-muted text-xs block"><b class="caret"></b></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="J_menuItem" href="person">个人资料</a>
                            </li>
                            <li><a class="J_menuItem" href="edit">编辑个人资料</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="loginout">安全退出</a>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element">实验室管理系统</div>
                </li>
                <?php if($user['authority']=='4'): ?>
                <li>
                    <a href="#">
                        <i class="fa fa fa-group"></i>
                        <span class="nav-label">用户管理</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="../User/user.html">用户列表</a>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>
                <li>
                    <a href="#">
                        <i class="fa fa fa-flask"></i>
                        <span class="nav-label">实验室设备管理</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="../Machine/machine.html">设备信息查询</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa fa-cubes"></i>
                        <span class="nav-label">耗材管理</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <?php if($user['authority']=='4' OR $user['authority']=='2'OR $user['authority']=='0'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/apply_purchase.html">耗材购买申请</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href="../Material/material_list.html">耗材购买申请记录</a>
                        </li>
                        <?php endif; if($user['authority']=='4' OR $user['authority']=='2'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/check_purchase.html">审核购买申请</a>
                        </li>
                        <?php endif; if($user['authority']=='4' OR $user['authority']=='3'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/disturb_material.html">分发耗材</a>
                        </li>
                        <?php endif; if($user['authority']=='4' OR $user['authority']=='2'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/check_receive.html">审核领取申请</a>
                        </li>
                        <?php endif; if($user['authority']=='4' OR $user['authority']=='3'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/into_repertory.html">耗材入库</a>
                        </li>
                        <?php endif; ?>
                        <li>
                            <a class="J_menuItem" href="../Material/receive_list.html">个人申领记录</a>
                        </li>
                        <?php if($user['authority']=='4' OR $user['authority']=='3' OR $user['authority']=='2' OR $user['authority']=='0'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/receive_material.html">耗材领取</a>
                        </li>
                        <?php endif; if($user['authority']=='4' OR $user['authority']=='3' OR $user['authority']=='2'): ?>
                        <li>
                            <a class="J_menuItem" href="../Material/search_material.html">耗材信息查询</a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php if($user['authority']=='4'OR $user['authority']=='1'): ?>
                <li>
                    <a href="#">
                        <i class="fa fa fa-calendar-check-o"></i>
                        <span class="nav-label">日志管理</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="../Worklog/work.html">查看工作日志</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href="../Worklog/repair.html">查看维修日志</a>
                        </li>

                    </ul>
                </li>
                <?php endif; ?>
                <li>
                    <a href="#">
                        <i class="fa fa-file-word-o"></i>
                        <span class="nav-label">生成word</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="../Table/count.html">统计表</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="J_menuItem" href="loginout.html"><i class="fa fa-times"></i><span class="nav-label">安全退出</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
<!--                        <div class="form-group">-->
<!--                            <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">-->
<!--                        </div>-->
                    </form>
                </div>
            </nav>
        </div>
        <div class="row content-tabs">
            <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i></button>
            <nav class="page-tabs J_menuTabs">
                <div class="page-tabs-content">
                    <a href="javascript:;" class="active J_menuTab" data-id="friend">首页</a>
                </div>
            </nav>
            <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
            </button>
            <div class="btn-group roll-nav roll-right">
                <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu dropdown-menu-right">
                    <li class="J_tabShowActive"><a>定位当前选项卡</a></li>
                    <li class="divider"></li>
                    <li class="J_tabCloseAll"><a>关闭全部选项卡</a></li>
                    <li class="J_tabCloseOther"><a>关闭其他选项卡</a></li>
                </ul>
            </div>
            <a href="loginout.html" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
        </div>
        <div class="row J_mainContent" id="content-main">
            <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="home.html" frameborder="0" seamless data-id="home"></iframe>
        </div>
    </div>
    <!--右侧部分结束-->
    <div class="footer">
        <div class="pull-right">&copy; 2021<a href="#" target="_blank">王宇哲</a></div>
    </div>
</div>
<script type="text/javascript">
    var BASE_URL = '/static/home/js/plugins/webuploader';
</script>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>