<?php /*a:3:{s:56:"D:\phpstudy_pro\lab\application/home/view\user\user.html";i:1612611439;s:63:"D:\phpstudy_pro\lab\application/home/view\user\base\header.html";i:1612501231;s:63:"D:\phpstudy_pro\lab\application/home/view\user\base\footer.html";i:1612008989;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<!--    <link href="/static/home/css/demo/webuploader-demo.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="http://fex.baidu.com/webuploader/css/demo.css">
<!--    <link href="https://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="/static/home/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>

  <body class="gray-bg">
  <div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>用户管理</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <div class="row row-lg">
          <div class="col-sm-12">
            <!-- Example Events -->
            <div class="example-wrap">
              <div class="example">
                <div class="btn-group" id="exampleTableEventsToolbar" role="group">
                  <button type="button" id="add" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                    增加
                  </button>
                  <button type="button" id="look" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                    查看
                  </button>
                  <button type="button" id="edit" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                    修改
                  </button>
                  <button type="button" id="delete" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-trash"  aria-hidden="true"></i>
                    批量删除
                  </button>
                </div>
                <table
                  id="userlist"
                  data-height="400"
                  data-mobile-responsive="true">
                  <thead>
                  <tr>
                    <th data-field="state" data-checkbox="true"></th>
                    <th data-field="Id">ID</th>
                    <th data-field="uid">用户编号</th>
                    <th data-field="name">姓名</th>
                    <th data-field="sex">性别</th>
                    <th data-field="authority">人员类型</th>
                    <th data-field="auth_lab">所管实验室</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>
            <!-- End Example Events -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Panel Other -->
  </div>
  <script type="text/javascript">
    var BASE_URL = '/static/home/js/plugins/webuploader';
</script>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>
  <script src="/static/home/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
  <script src="/static/home/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
  <script src="/static/home/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
  <script src="/static/home/js/layui/layui.all.js"></script>
  <!-- Peity -->
  <script src="/static/home/js/demo/user.js"></script>

  </body>

  </html>
