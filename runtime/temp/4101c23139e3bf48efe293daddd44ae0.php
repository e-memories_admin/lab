<?php /*a:1:{s:71:"D:\phpstudy_pro\lab\application/home/view\table\insert_count_table.html";i:1618227695;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
   <link href="/static/home/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/home/css/plugins/chosen/chosen.css">
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>


<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>添加实验列表</h5>
            </div>
            <div class="ibox-content">
                <form method="post" action="into_count_table" class="form-horizontal" id="valid_form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">实验名称：</label>
                        <div class="col-sm-10">
                            <input type="text" name="experiment_name" placeholder="请输入实验名称" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">学时数：</label>
                        <div class="col-sm-10">
                            <input type="number" name="hours" plahceholder="请输入学时数" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">实验类别：</label>
                        <div class="col-sm-10">
                            <input type="text" name="experiment_type" placeholder="请输入实验类别" class="form-control">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">要求：</label>
                        <div class="col-sm-10">
                            <input type="text" name="requirement" placeholder="请输入实验要求" class="form-control">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">实验专业：</label>
                        <div class="col-sm-10">
                            <input type="text" name="experiment_major" placeholder="请输入实验专业" class="form-control">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">班级：</label>
                        <div class="col-sm-10">
                            <input type="text" name="class" placeholder="请输入班级" class="form-control">
                        </div>
                    </div>



                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">实验者类别：</label>
                        <div class="col-sm-10">
                            <input type="text" name="experimenter_type" placeholder="请输入实验员类别" class="form-control">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">人数：</label>
                        <div class="col-sm-10">
                            <input type="number" name="total_number" placeholder="请输入人数" class="form-control">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">相同实验组数：</label>
                        <div class="col-sm-10">
                            <input type="number" name="similar_num" placeholder="请输入相同组数" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">每组人数：</label>
                        <div class="col-sm-10">
                            <input type="number" name="each_num" placeholder="请输入实验类别" class="form-control">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">开课日期：</label>
                        <div class="col-sm-10">
                            <input type="text" id="date" name="class_date" class="form-control m-b">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">开课节次：</label>
                        <div class="col-sm-10">
                            <select data-placeholder="选择开课节次" name="section[]" class="form-control m-b chosen1" multiple  tabindex="4">
                                <option value="">请选择节次</option>
                                <option value="1" hassubinfo="true">第1节</option>
                                <option value="2" hassubinfo="true">第2节</option>
                                <option value="3" hassubinfo="true">第3节</option>
                                <option value="4" hassubinfo="true">第4节</option>
                                <option value="5" hassubinfo="true">第5节</option>
                                <option value="6" hassubinfo="true">第6节</option>
                                <option value="7" hassubinfo="true">第7节</option>
                                <option value="8" hassubinfo="true">第8节</option>
                                <option value="9" hassubinfo="true">第9节</option>
                                <option value="10" hassubinfo="true">第10节</option>
                                <option value="11" hassubinfo="true">第11节</option>
                                <option value="12" hassubinfo="true">第12节</option>
                                <option value="13" hassubinfo="true">第13节</option>
                                <option value="14" hassubinfo="true">第14节</option>
                            </select>
                        </div>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label class="col-sm-2 control-label">周数：</label>-->
<!--                        <div class="col-sm-10">-->
<!--                            <select data-placeholder="选择周数" name="ww" class="form-control m-b chosen-select" multiple  tabindex="4">-->
<!--                                <option value="">请选择周数</option>-->
<!--                                <option value="1" hassubinfo="true">第1周</option>-->
<!--                                <option value="2" hassubinfo="true">第2周</option>-->
<!--                                <option value="3" hassubinfo="true">第3周</option>-->
<!--                                <option value="4" hassubinfo="true">第4周</option>-->
<!--                                <option value="5" hassubinfo="true">第5周</option>-->
<!--                                <option value="6" hassubinfo="true">第6周</option>-->
<!--                                <option value="7" hassubinfo="true">第7周</option>-->
<!--                                <option value="8" hassubinfo="true">第8周</option>-->
<!--                                <option value="9" hassubinfo="true">第9周</option>-->
<!--                                <option value="10" hassubinfo="true">第10周</option>-->
<!--                                <option value="11" hassubinfo="true">第11周</option>-->
<!--                                <option value="12" hassubinfo="true">第12周</option>-->
<!--                                <option value="13" hassubinfo="true">第13周</option>-->
<!--                                <option value="14" hassubinfo="true">第14周</option>-->
<!--                                <option value="15" hassubinfo="true">第15周</option>-->
<!--                                <option value="16" hassubinfo="true">第16周</option>-->
<!--                                <option value="17" hassubinfo="true">第17周</option>-->
<!--                                <option value="18" hassubinfo="true">第18周</option>-->
<!--                                <option value="19" hassubinfo="true">第19周</option>-->
<!--                                <option value="20" hassubinfo="true">第20周</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">备注：</label>
                        <div class="col-sm-10">
                            <textarea value="" name="comment" class="form-control m-b"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="father_table_id" value="<?php echo htmlentities($Id); ?>">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">新增</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script src="/static/home/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>
<script src="/static/home/js/plugins/chosen/chosen.jquery.js"></script>
<!--<script src="/static/home/js/demo/form-advanced-demo.min.js"></script>-->
<script>
    $('.chosen1').chosen({
        no_results_text: "没有找到结果！",//搜索无结果时显示的提示
        search_contains:true,//关键字模糊搜索。设置为true，只要选项包含搜索词就会显示；设置为false，则要求从选项开头开始匹配
        allow_single_deselect:false, //单选下拉框是否允许取消选择。如果允许，选中选项会有一个x号可以删除选项
        disable_search: false, //禁用搜索。设置为true，则无法搜索选项。
        disable_search_threshold: 0, //当选项少等于于指定个数时禁用搜索。
        display_disabled_options: true,
        single_backstroke_delete: true, //false表示按两次删除键才能删除选项，true表示按一次删除键即可删除
        case_sensitive_search: false, //搜索大小写敏感。此处设为不敏感
        group_search: false, //选项组是否可搜。此处搜索不可搜
        include_group_label_in_selected: true //选中选项是否显示选项分组。false不显示，true显示。默认false。
    });
    $('#date').datepicker({
        language:'zh-CN',
        //autoclose:true,
        clearBtn: true,
        viewMode:"days",
        format:"mm-dd",
        multidate: true,//允许日期多选
        //multidateSeparator: ''//多个时间以逗号隔开
    })
</script>