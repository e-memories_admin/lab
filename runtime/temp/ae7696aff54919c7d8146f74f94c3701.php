<?php /*a:3:{s:62:"D:\phpstudy_pro\lab\application/home/view\machine\machine.html";i:1612877849;s:66:"D:\phpstudy_pro\lab\application/home/view\machine\base\header.html";i:1612501231;s:66:"D:\phpstudy_pro\lab\application/home/view\machine\base\footer.html";i:1612008989;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<!--    <link href="/static/home/css/demo/webuploader-demo.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="http://fex.baidu.com/webuploader/css/demo.css">
<!--    <link href="https://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="/static/home/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>

<link href="/static/home/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
  <body class="gray-bg">
  <div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>设备管理</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <div class="row row-lg">
          <div class="pull-left" >
            <select class="form-control m-b" id="select_type" name="account">
              <option value="date">时间</option>
              <option value="asset_id" selected>资产编号</option>
              <option value="asset_name">设备名称</option>
              <option value="manager">管理人</option>
              <option value="depositary">存放位置</option>
              <option value="model">型号</option>
              <option value="status">状态</option>
              <option value="comment">备注</option>
            </select>
          </div>
          <div class="pull-left search" id="other_search">
            <input class="form-control input-outline" id="other_data" type="text" placeholder="搜索">
          </div>
          <div class="pull-left" id="date_search" hidden>
            <div class="input-group input-medium date-picker input-daterange" data-date-format="yyyy-mm-dd">
              <input name="dtBegin" class="form-control" id="date_from" style="font-size: 13px;" type="text" value="">
              <span class="input-group-addon">到</span>
              <input name="dtEnd" class="form-control" id="date_to" style="font-size: 13px;" type="text" value="">
            </div>
          </div>
          <div class="col-sm-12">
            <!-- Example Events -->

            <div class="example-wrap">

              <div class="example">
                <div class="btn-group hidden-xs" id="exampleTableEventsToolbar" role="group">
                  <button type="button" id="add" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                    新增
                  </button>
                  <button type="button" id="add_all" class="btn btn-outline btn-default">
                    <i class="fa fa-check-square-o"  aria-hidden="true"></i>
                    批量新增
                  </button>
                  <button type="button" id="export" class="btn btn-outline btn-default">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                    导出
                  </button>
                  <button type="button" id="look" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                    查看
                  </button>
                  <button type="button" id="edit" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                    编辑
                  </button>
                  <button type="button" id="delete" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-trash"  aria-hidden="true"></i>
                    删除
                  </button>
                </div>

                <table
                  id="machinelist"
                  data-height="400"
                  data-mobile-responsive="true">

                  <thead>
                  <tr>
                    <th data-field="state" data-checkbox="true"></th>
                    <th data-field="asset_id">资产编号</th>
                    <th data-field="asset_name">设备名</th>
                    <th data-field="model">型号</th>
                    <th data-field="depositary" data-formatter="lab">所属实验室</th>
                    <th data-field="status">状态</th>
                    <th data-field="manager">管理人</th>
                    <th data-field="time">购置时间</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>
            <!-- End Example Events -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Panel Other -->
  </div>
  <script type="text/javascript">
    var BASE_URL = '/static/home/js/plugins/webuploader';
</script>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>
  <script>
    var authority=<?php echo htmlentities($user['authority']); ?>;
    function lab(value,row,index) {
      var a = "";
      if(value == '<?php echo htmlentities($user['auth_lab']); ?>') {
        var a = '<span style="color:#2fff00">'+value+'</span>';
      }else{
        var a = value;
      }
      return a;
    }
    $('#export').on('click',function (){
      window.location.replace('export_machine_data.html');
    });

  </script>
  <script src="/static/home/js/plugins/datapicker/bootstrap-datepicker.js"></script>

  <script src="/static/home/js/plugins/bootstrap-table/bootstrap-table.js"></script>
  <script src="/static/home/js/plugins/export/bootstrap-table-export.js"></script>
  <script src="/static/home/js/plugins/export/tableExport.js"></script>
  <script src="/static/home/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
  <script src="/static/home/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
  <script src="/static/home/js/layui/layui.all.js"></script>
  <!--以XLSX（Excel 2007+ XML格式）格式导出表（SheetJS）-->

  <!-- Peity -->
  <script src="/static/home/js/demo/machine.js"></script>
  <script>
    $(function () {
      $('#data.input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
      })
    });
    $(".date-picker").datepicker({
      language: "zh-CN",
      autoclose: true
    });
  </script>
  </body>

  </html>
