<?php /*a:3:{s:58:"D:\phpstudy_pro\lab\application/home/view\index\index.html";i:1613905603;s:64:"D:\phpstudy_pro\lab\application/home/view\index\base\header.html";i:1612501231;s:64:"D:\phpstudy_pro\lab\application/home/view\index\base\footer.html";i:1612008989;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<!--    <link href="/static/home/css/demo/webuploader-demo.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="http://fex.baidu.com/webuploader/css/demo.css">
<!--    <link href="https://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="/static/home/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="row border-bottom white-bg dashboard-header">
    <div class="col-sm-5">
        <h1>
           实验室信息管理系统
        </h1>
        <h3>实验室管理系统为实验室的管理提供快捷方便的服务。<br>它包括<b>实验室设备管理，实验耗材管理，日志管理</b>等功能。</h3>
        <hr>
        <h2>
            程序的优势
        </h2>
        <blockquote class="text-info" style="font-size:14px">
            当你遇到大量数据实验设备数据时，你会感到吃力，因为在以往，你需要在电脑面前坐一下午，将数据一个个录入，这是一个体力活
            <br>
            <h4 class="text-danger">所以，我们有批量导入功能，大大减少工作量</h4>
            <br>当你在申购实验耗材时，有时竟然忘记了了申请的耗材的信息，于是打开耗材查询，又看了一遍，在输入日志时，有时候想要输入的内容是之前输入过的，却还要耐着性子重新输入一遍
            <h4 class="text-danger">所以，我们有联想输入功能，节约你的时间</h4>
            <br>当看向工作日志时，各种日志让你眼花缭乱
            <h4 class="text-danger">所以，我们对日志绑定了图标，一目了然</h4>
            <br>当你在审核各种申请时，几百条的申请实在是让人心烦意乱
            <h4 class="text-danger">所以，我们有批量审核功能</h4>
            <br>每每到了年终，当需要之前的日志或者其他信息时，往往会打开网页，一个个复制到自己的文档中，这确实很闹心
            <br>当你在审核各种申请时，几百条的申请实在是让人心烦意乱
            <h4 class="text-danger">所以，我们有批量审核功能</h4>
            <br>
            <h4 class="text-danger">除此之外，响应式的界面，连贯的操作，友好的界面交互...<br>都是我们的优势之处</h4>
        </blockquote>
    </div>
    <div class="col-sm-5">
        <div class="ibox-content no-padding">
            <div class="panel-body">
                <div class="panel-group" id="version">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#version" href="#1">用户管理</a>
                            </h5>
                        </div>
                        <div id="1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <ol>
                                    <li>1.用户增加</li>
                                    <li>2.用户信息修改</li>
                                    <li>3.用户信息查询</li>
                                    <li>4.批量删除用户</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#version" href="#3">实验室设备管理</a>
                            </h5>
                        </div>
                        <div id="3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ol>
                                    <li>1.设备增加</li>
                                    <li>2.设备信息修改</li>
                                    <li>3.设备信息查询</li>
                                    <li>4.批量删除设备</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#version" href="#2">耗材管理</a>
                            </h5>
                        </div>
                        <div id="2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ol>
                                    <li>1.耗材采购申请</li>
                                    <li>2.耗材领取</li>
                                    <li>3.耗材信息查询</li>
                                    <li>4.批量删除耗材</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#version" href="#4">日志管理</a>
                            </h5>
                        </div>
                        <div id="4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ol>
                                    <li>1.日志生成</li>
                                    <li>2.日志信息查询</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var BASE_URL = '/static/home/js/plugins/webuploader';
</script>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>