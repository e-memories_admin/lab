<?php /*a:3:{s:66:"D:\phpstudy_pro\lab\application/home/view\machine\machine_add.html";i:1612757690;s:66:"D:\phpstudy_pro\lab\application/home/view\machine\base\header.html";i:1612501231;s:66:"D:\phpstudy_pro\lab\application/home/view\machine\base\footer.html";i:1612008989;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<!--    <link href="/static/home/css/demo/webuploader-demo.min.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="http://fex.baidu.com/webuploader/css/demo.css">
<!--    <link href="https://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="/static/home/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>新增设备</h5>
            </div>
            <div class="ibox-content">
                <form method="post" action="" class="form-horizontal" id="valid_form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">资产编号:</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="请输入内部编号" value="" name="asset_id" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">内部编号:</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="请输入内部编号" value="" name="inside_id" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">设备名:</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="请输入设备名" name="asset_name" value="" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">单价:</label>
                        <div class="col-sm-10">
                            <input type="number" step="0.01"  name="unit_price" value="" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">管理人:</label>
                        <div class="col-sm-10">
                            <input type="text" value="" name="manager" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">存放位置:</label>
                        <div class="col-sm-10">
                            <input type="text" value="" name="depositary" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">型号:</label>
                        <div class="col-sm-10">
                            <input type="text" value="" name="model" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">规格：</label>
                        <div class="col-sm-10">
                            <input type="text" value="" name="standard" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">存放时间:</label>
                        <div class="col-sm-10">
                            <input type="datetime-local" value="" name="time" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">状态 :</label>
                        <div class="col-sm-10">
                            <input type="text" value="" name="status" class="form-control">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注:</label>
                        <div class="col-sm-10">
                            <textarea type="datetime-local" value="" name="comment" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">新增</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var BASE_URL = '/static/home/js/plugins/webuploader';
</script>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/plugins/layer/layer.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>