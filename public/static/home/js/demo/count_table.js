var $edit=$('#edit'),
    $add=$('#add'),
    $delete=$('#delete'),
    $table=$('#machinelist');

$edit.prop('disabled',true);
$add.prop('disabled',false);
$delete.prop('disabled',true);
(function(document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function() {
        function delete_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'delete_count_table.html',
                data:ids,
                dataType:'JSON',
                success:function (){
                    layer.msg('删除成功');
                    $table.bootstrapTable('refresh');
                },
                error:function (){
                    layer.msg('删除失败');
                }
            })
        }
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $add.on('click',function (){
            //if (isInArray([4,3],authority)){
            layer.open({
                type: 2,
                title: '增加新课程',
                shadeClose: false,
                shade: 0.8,
                area: ['50%', '60%'],
                content: 'insert_count_table.html?Id='+$Id,
                end:function (){
                    $table.bootstrapTable('refresh');
                }
            });
            //}else{
            //   layer.msg('你无权操作');
            //}

        })
        $delete.on('click',function (){
                var ids=getSelections();
                if (ids.length==0){
                    layer.msg('请先选择至少一个课程');
                }else{
                    layer.confirm('确定删除这'+ids.length+'个课程？',{
                        btn:['是','否']
                    },function (){
                        delete_all(ids);
                    })
                }

        })
        $table.bootstrapTable({
            url: "count_table_json.html?Id="+$Id,
            dataType:'json',
            method:'get',
            cardView:true,
            search: true,
            pagination: true,
            showRefresh: true,
            //showToggle: true,
            showColumns: true,
            clickToSelect: true,
            //showExport: true,
            //exportDataType: "base",
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                 toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list-alt'
            },
            columns: [{
                field: 'state',
                checkbox:true
            },{
                title:"实验名称",
                field:"experiment_name",
                editable:{
                    type:"text"
                }
            },{
                title:"学时",
                field:"hours",
                editable: {
                    type: "text"
                }
            },{
                title:"实验类别",
                field:"experiment_type",
                editable:{
                    type:"select",
                    source:[
                        {value:'专业课',text:'专业课'},
                        {value:'基础课',text:'基础课'},
                        {value:'专业基础课',text:'专业基础课'}
                    ]
                }
            },{
                title: "要求",
                field:"requirement",
                editable:{
                    type:"select",
                    source:[
                        {value:'必做',text:'必做'},
                        {value:'选做',text:'选做'},
                        {value:'其他',text:'其他'}
                    ]
                }
            },{
                title: "实验专业",
                field:"experiment_major",
                editable:{
                    type:"text"
                }
            },{
                title: "班级",
                field:"class",
                editable:{
                    type:"text"
                }
            },{
                title: "实验者类别",
                field:"experimenter_type",
                editable:{
                    type:"select",
                    source:[
                        {value:'本科生',text:'本科生'},
                        {value:'专科生',text:'专科生'},
                        {value:'其他',text:'其他'}
                    ]
                }
            },{
                title: "人数",
                field:"total_number",
                editable:{
                    type:"number"
                }
            },{
                title: "相同实验组数",
                field:"similar_num",
                editable:{
                    type:"number"
                }
            },{
                title: "每组人数",
                field:"each_num",
                editable:{
                    type:"number"
                }
            },{
                title: "开出日期",
                field:"class_date",
                editable:{
                    type:"text"
                }
            },{
                title: "开课节次",
                field:"section",
                editable:{
                    type:"text"
                    // select2:{
                    //     multiple:true,
                    //     allowClear:true,
                    //     placeholder: '请选择节次',
                    //     emptytext:false
                    // },
                    // source:[
                    //     {value:1,text:"第1节"},
                    //     {value:2,text:"第2节"},
                    //     {value:3,text:"第3节"},
                    //     {value:4,text:"第4节"},
                    //     {value:5,text:"第5节"},
                    //     {value:6,text:"第6节"},
                    //     {value:7,text:"第7节"},
                    //     {value:8,text:"第8节"},
                    //     {value:9,text:"第9节"},
                    //     {value:10,text:"第10节"},
                    //     {value:11,text:"第11节"},
                    //     {value:12,text:"第12节"},
                    //     {value:13,text:"第13节"},
                    //     {value:14,text:"第14节"}
                    // ]
                }
            },{
                title: "备注",
                field:"comment",
                editable:{
                    type:"textarea"
                }
            }],
            onEditableSave:function (field, row, oldValue, $el) {
                row['field']=field;
                $.ajax({
                    type: "post",
                    url: "edit_count_table.html",
                    data: row,
                    dataType: 'JSON',
                    success: function (data, status) {
                        if (status == "success") {
                            layer.msg("编辑成功");
                        }
                    },
                    error: function () {
                        layer.msg("编辑失败");
                    },
                    complete: function () {

                    }

                });
            }
            // //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
            // //basic', 'all', 'selected'.
            // exportTypes:['json','xml','csv','txt','sql','doc','excel'],	    //导出类型
            // //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            // exportOptions:{
            //     //ignoreColumn: [0,0],            //忽略某一列的索引
            //     fileName: '数据导出',              //文件名称设置
            //     worksheetName: 'Sheet1',          //表格工作区名称
            //     tableName: '商品数据表',
            //     //onMsoNumberFormat: 'DoOnMsoNumberFormat'
            // }
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool=!(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length==1
            )
            $edit.prop('disabled',bool);
            $look.prop('disabled',bool);
            var bool2=!(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled',bool2)
        })
    })();
})(document, window, jQuery);
