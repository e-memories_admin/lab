
/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

function cellStyle(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function rowStyle(row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function scoreSorter(a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}

function nameFormatter(value) {
    return value + '<i class="icon wb-book" aria-hidden="true"></i> ';
}

function starsFormatter(value) {
    return '<i class="icon wb-star" aria-hidden="true"></i> ' + value;
}

function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 100,
        page: 1
    };
}

function buildTable($el, cells, rows) {
    var i, j, row,
        columns = [],
        data = [];

    for (i = 0; i < cells; i++) {
        columns.push({
            field: '字段' + i,
            title: '单元' + i
        });
    }
    for (i = 0; i < rows; i++) {
        row = {};
        for (j = 0; j < cells; j++) {
            row['字段' + j] = 'Row-' + i + '-' + j;
        }
        data.push(row);
    }
    $el.bootstrapTable('destroy').bootstrapTable({
        columns: columns,
        data: data,
        iconSize: 'outline',
        icons: {
            columns: 'glyphicon-list'
        }
    });
}

var $table=$('#materiallist'),
    $pass=$('#pass'),
    $fail=$('#fail');
(function(document, window, $) {
    // Example Bootstrap Table Events
    // ------------------------------
    $pass.prop('disabled',true);
    $fail.prop('disabled',true);


    (function() {
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        function fail_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'fail_all_apply.html?do=fail',
                data:ids,
                dataType:'JSON',
                success:function (data){
                    layer.msg('驳回完成，成功'+data.success+'条，失败'+data.error+'条');
                },
                error:function (){
                    layer.msg('驳回失败');
                }
            })
        }
        function pass_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'pass_all_apply.html?do=fail',
                data:ids,
                dataType:'JSON',
                success:function (data){
                    layer.msg('通过完成，成功'+data.success+'条，失败'+data.error+'条');
                },
                error:function (){
                    layer.msg('通过失败');
                }
            })
        }
        $pass.on('click',function (){
            var ids=getSelections();
            if (ids.length==0){
                layer.msg('请先选择至少一个记录');
            }else{
                layer.confirm('确定通过这'+ids.length+'个记录？',{
                    btn:['是','否']
                },function (){
                    pass_all(ids);
                    $table.bootstrapTable('refresh');
                })
            }

        });
        $fail.on('click',function (){
            var ids=getSelections();
            if (ids.length==0){
                layer.msg('请先选择至少一个记录');
            }else{
                layer.confirm('确定驳回这'+ids.length+'个记录？',{
                    btn:['是','否']
                },function (){
                    fail_all(ids);
                    $table.bootstrapTable('refresh');
                })
            }

        });
        $table.bootstrapTable({
            url: "all_material_list_json.html",
            dataType:'json',
            method:'post',
            search: true,
            pagination: true,
            clickToSelect: true,
            showRefresh: true,
            showToggle: true,
            showColumns: true,
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list'
            },
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool=!(
                $table.bootstrapTable('getSelections').length
            )
            $pass.prop('disabled',bool);
            $fail.prop('disabled',bool);

        })
    })();
})(document, window, jQuery);
