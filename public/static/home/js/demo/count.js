var $edit=$('#edit'),
    $add=$('#add'),
    $delete=$('#delete'),
    $table=$('#machinelist');
$edit.prop('disabled',true);
$add.prop('disabled',false);
$delete.prop('disabled',true);
(function(document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function() {
        function delete_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'delete_count.html',
                data:ids,
                dataType:'JSON',
                success:function (){
                    layer.msg('删除成功');
                    $table.bootstrapTable('refresh');
                },
                error:function (){
                    layer.msg('删除失败');
                }
            })
        }
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $add.on('click',function (){
            //if (isInArray([4,3],authority)){
                layer.open({
                    type: 2,
                    title: '增加新课程',
                    shadeClose: false,
                    shade: 0.8,
                    area: ['50%', '60%'],
                    content: 'insert_count.html',
                    end:function (){
                        $table.bootstrapTable('refresh');
                    }
                });
            //}else{
            //   layer.msg('你无权操作');
            //}

        })
        $delete.on('click',function (){
                var ids=getSelections();
                if (ids.length==0){
                    layer.msg('请先选择至少一个表');
                }else{
                    layer.confirm('确定删除这'+ids.length+'个表？',{
                        btn:['是','否']
                    },function (){
                        delete_all(ids);
                    })
                }
        })
        $table.bootstrapTable({
            url: "count_json.html",
            dataType:'json',
            method:'post',
            search: true,
            pagination: true,
            showRefresh: true,
            // showToggle: true,
            showColumns: true,
            clickToSelect: true,
            showExport: true,
            exportDataType: "base",
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list-alt'
            },
            columns: [{
                field:'state',
                checkbox:true
            },{
                field:'course',
                title:'课程名称',
                editable:{
                    type:"text"
                }
            },{
                field:'college',
                title:'开课单位',
                editable: {
                    type: "text"
                }
            },{
                field:'experimenter',
                title:'负责实验员',
                editable: {
                    type: "text"
                }
            },{
                field:'laboratory',
                title:'开展实验室',
                editable: {
                    type: "text"
                }
            },{
                field:'teacher',
                title:'授课老师',
                editable: {
                    type: "text"
                }
            },{
                field:'class_date',
                title:'课程时间',
                editable: {
                    type: "text"
                }
            },{
                field:'id',
                title:'管理实验表',
                formatter:'op',

            }],
            onEditableSave:function (field,row,oldValue,$el){
                row["field"]=field;
                $.ajax({
                    type:"post",
                    url:"edit_count.html",
                    data:row,
                    dataType:"JSON",
                    success:function (data,status){
                        if(status=="success"){
                            layer.msg("编辑成功");
                        }
                    },
                    error:function (){
                        layer.msg("编辑失败");
                    },
                    complete: function () {

                    }
                })
            }
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool=!(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length==1
            )
            $edit.prop('disabled',bool);
            var bool2=!(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled',bool2)
        })
    })();
})(document, window, jQuery);
