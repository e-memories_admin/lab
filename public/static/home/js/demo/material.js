
/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

function cellStyle(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function rowStyle(row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function scoreSorter(a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}

function nameFormatter(value) {
    return value + '<i class="icon wb-book" aria-hidden="true"></i> ';
}

function starsFormatter(value) {
    return '<i class="icon wb-star" aria-hidden="true"></i> ' + value;
}

function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 100,
        page: 1
    };
}

function buildTable($el, cells, rows) {
    var i, j, row,
        columns = [],
        data = [];

    for (i = 0; i < cells; i++) {
        columns.push({
            field: '字段' + i,
            title: '单元' + i
        });
    }
    for (i = 0; i < rows; i++) {
        row = {};
        for (j = 0; j < cells; j++) {
            row['字段' + j] = 'Row-' + i + '-' + j;
        }
        data.push(row);
    }
    $el.bootstrapTable('destroy').bootstrapTable({
        columns: columns,
        data: data,
        iconSize: 'outline',
        icons: {
            columns: 'glyphicon-list'
        }
    });
}

function ajaxRequest(params){
    $.ajax({
        url: 'material_json.html',
        type: 'POST',
        dataType: 'json',
        success:function (rs){
            console.log(rs);
            var message=rs.array;
            params.success({
                total:rs.total,
                rows:message
            })
        },
        error:function (rs){
            console.log(rs)
        }
    })
}
var $table=$('#materiallist'),
    $look=$('#look');
$look.prop('disabled',true);
(function(document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function() {
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $look.on('click',function (){
            var row=getSelections()[0];
            var material_id=row.material_id;
            layer.open({
                type: 2,
                title: '查看耗材库存信息',
                shadeClose: false,
                shade: 0.8,
                area: ['50%', '60%'],
                content: 'material_look.html?material_id='+material_id,
            });
        })
        $table.bootstrapTable({
            url: "material_json.html",
            dataType:'json',
            method:'post',
            search: true,
            pagination: true,
            showRefresh: true,

            showColumns: true,

            clickToSelect: true,
            showExport: true,
            exportDataType: "base",


            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                // toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list-alt',
                export: 'glyphicon-export icon-share'
            },
            //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
            //basic', 'all', 'selected'.
            exportTypes:['json','xml','csv','txt','sql','doc','excel'],	    //导出类型
            //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            exportOptions:{
                //ignoreColumn: [0,0],            //忽略某一列的索引
                fileName: '数据导出',              //文件名称设置
                worksheetName: 'Sheet1',          //表格工作区名称
                tableName: '商品数据表',
                //onMsoNumberFormat: 'DoOnMsoNumberFormat'
            }
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool=!(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length==1
            )
            $look.prop('disabled',bool);

        })
    })();
})(document, window, jQuery);
