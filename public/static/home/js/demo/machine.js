
/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

function cellStyle(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function rowStyle(row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function scoreSorter(a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}

function nameFormatter(value) {
    return value + '<i class="icon wb-book" aria-hidden="true"></i> ';
}

function starsFormatter(value) {
    return '<i class="icon wb-star" aria-hidden="true"></i> ' + value;
}

function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 100,
        page: 1
    };
}

function buildTable($el, cells, rows) {
    var i, j, row,
        columns = [],
        data = [];

    for (i = 0; i < cells; i++) {
        columns.push({
            field: '字段' + i,
            title: '单元' + i
        });
    }
    for (i = 0; i < rows; i++) {
        row = {};
        for (j = 0; j < cells; j++) {
            row['字段' + j] = 'Row-' + i + '-' + j;
        }
        data.push(row);
    }
    $el.bootstrapTable('destroy').bootstrapTable({
        columns: columns,
        data: data,
        iconSize: 'outline',
        icons: {
            columns: 'glyphicon-list'
        }
    });
}

function ajaxRequest(params){
    $.ajax({
        url: 'demo.php',
        type: 'POST',
        dataType: 'json',
        success:function (rs){
            console.log(rs);
            var message=rs.array;
            params.success({
                total:rs.total,
                rows:message
            })
        },
        error:function (rs){
            console.log(rs)
        }
    })
}
function isInArray(arr,value){
    for(var i = 0; i < arr.length; i++){
        if(value === arr[i]){
            return true;
        }
    }
    return false;
}
var $edit=$('#edit'),
    $add=$('#add'),
    $delete=$('#delete'),
    $table=$('#machinelist'),
    $look=$('#look'),
    $add_all=$('#add_all'),
    $select=$('#select_type'),
    $other=$('#other_data'),
    $from_data=$('#date_from'),
    $to_data=$('#date_to');

$look.prop('disabled',true);
$edit.prop('disabled',true);
$add.prop('disabled',false);
$delete.prop('disabled',true);
(function(document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function() {
        function delete_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'delete_machine.html?do=delete',
                data:ids,
                dataType:'JSON',
                success:function (){
                    layer.msg('删除成功');
                    $table.bootstrapTable('refresh');
                },
                error:function (){
                    layer.msg('删除失败');
                }
            })
        }
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $add.on('click',function (){
            if (isInArray([4,3],authority)){
                layer.open({
                    type: 2,
                    title: '增加新设备',
                    shadeClose: false,
                    shade: 0.8,
                    area: ['50%', '60%'],
                    content: 'add_machine.html',
                    end:function (){
                        $table.bootstrapTable('refresh');
                    }
                });
            }else{
                layer.msg('你无权操作');
            }

        })
        $add_all.on('click',function (){
            if (isInArray([4,3],authority)){
                layer.open({
                    type: 2,
                    title: '导入设备',
                    shadeClose: false,
                    shade: 0.8,
                    area: ['50%', '25%'],
                    content: 'add_all.html',
                    end:function (){
                        $table.bootstrapTable('refresh');
                    }
                });
            }else{
                layer.msg('你无权操作');
            }

        })
        $look.on('click',function (){
            if (isInArray([4,3,2,1,0],authority)){
                var row=getSelections()[0];
                var asset_id=row.asset_id;
                layer.open({
                    type: 2,
                    title: '查看设备信息',
                    shadeClose: false,
                    shade: 0.8,
                    area: ['50%', '60%'],
                    content: 'look_machine.html?asset_id='+asset_id,
                });
            }else{
                layer.msg('你无权操作');
            }

        })
        $edit.on('click',function (){
            if (isInArray([4,3,1],authority)){
                var row=getSelections()[0];
                var asset_id=row.asset_id;
                layer.open({
                    type: 2,
                    title: '修改设备信息',
                    shadeClose: false,
                    shade: 0.8,
                    area: ['50%', '60%'],
                    content: 'edit_machine.html?asset_id=' + asset_id,
                    end:function (){
                        $table.bootstrapTable('refresh');
                    }
                });
            }else{
                layer.msg('你无权操作');
            }

        })
        $delete.on('click',function (){
            if (isInArray([4,3],authority)){
                var ids=getSelections();
                if (ids.length==0){
                    layer.msg('请先选择至少一个设备');
                }else{
                    layer.confirm('确定删除这'+ids.length+'个设备？',{
                        btn:['是','否']
                    },function (){
                        delete_all(ids);
                    })
                }
            }else{
                layer.msg('你无权操作');
            }
        })
        $select.on('change',function (){
            if($select.val()=='date'){
                $('#date_search').prop('hidden',false);
                $('#other_search').prop('hidden',true);
            }else{
                $('#date_search').prop('hidden',true);
                $('#other_search').prop('hidden',false);
            }
        });
        $other.on('input',function (){
            var type=$select.val();
            var url= "machine_json.html?field="+type+"&content="+$other.val();
            var opt={
                url:url
            };
            $table.bootstrapTable('refresh',opt)
        });
        $from_data.on('change',function (){
            var type=$select.val();
            var url= "machine_json.html?field="+type+"&from_date="+$from_data.val()+"&to_date="+$to_data.val();
            var opt={
                url:url
            };
            $table.bootstrapTable('refresh',opt)
        });
        $to_data.on('change',function (){
            var type=$select.val();
            var url= "machine_json.html?field="+type+"&from_date="+$from_data.val()+"&to_date="+$to_data.val();
            var opt={
                url:url
            };
            $table.bootstrapTable('refresh',opt)
        });
        $table.bootstrapTable({
            url: "machine_json.html",
            dataType:'json',
            method:'post',
            search: false,
            pagination: true,
            showRefresh: true,
            // showToggle: true,
            showColumns: true,

            clickToSelect: true,
            showExport: true,
            exportDataType: "base",

            
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                // toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list-alt',
                export: 'glyphicon-export icon-share'
            },
                          //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
                          //basic', 'all', 'selected'.
            exportTypes:['json','xml','csv','txt','sql','doc','excel'],	    //导出类型
            //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            exportOptions:{
                //ignoreColumn: [0,0],            //忽略某一列的索引
                fileName: '数据导出',              //文件名称设置
                worksheetName: 'Sheet1',          //表格工作区名称
                tableName: '商品数据表',
                //onMsoNumberFormat: 'DoOnMsoNumberFormat'
            }
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool=!(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length==1
            )
            $edit.prop('disabled',bool);
            $look.prop('disabled',bool);
            var bool2=!(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled',bool2)
        })
    })();
})(document, window, jQuery);
