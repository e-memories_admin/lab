/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

function cellStyle(value, row, index) {
  var classes = ['active', 'success', 'info', 'warning', 'danger'];

  if (index % 2 === 0 && index / 2 < classes.length) {
    return {
      classes: classes[index / 2]
    };
  }
  return {};
}

function rowStyle(row, index) {
  var classes = ['active', 'success', 'info', 'warning', 'danger'];

  if (index % 2 === 0 && index / 2 < classes.length) {
    return {
      classes: classes[index / 2]
    };
  }
  return {};
}

function scoreSorter(a, b) {
  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
}

function nameFormatter(value) {
  return value + '<i class="icon wb-book" aria-hidden="true"></i> ';
}

function starsFormatter(value) {
  return '<i class="icon wb-star" aria-hidden="true"></i> ' + value;
}

function queryParams() {
  return {
    type: 'owner',
    sort: 'updated',
    direction: 'desc',
    per_page: 100,
    page: 1
  };
}

function buildTable($el, cells, rows) {
  var i, j, row,
    columns = [],
    data = [];

  for (i = 0; i < cells; i++) {
    columns.push({
      field: '字段' + i,
      title: '单元' + i
    });
  }
  for (i = 0; i < rows; i++) {
    row = {};
    for (j = 0; j < cells; j++) {
      row['字段' + j] = 'Row-' + i + '-' + j;
    }
    data.push(row);
  }
  $el.bootstrapTable('destroy').bootstrapTable({
    columns: columns,
    data: data,
    iconSize: 'outline',
    icons: {
      columns: 'glyphicon-list'
    }
  });
}

function ajaxRequest(params){
  $.ajax({
    url: 'demo.php',
    type: 'POST',
    dataType: 'json',
    success:function (rs){
      console.log(rs);
      var message=rs.array;
      params.success({
        total:rs.total,
        rows:message
      })
    },
    error:function (rs){
      console.log(rs)
    }
  })
}
var $edit=$('#edit'),
    $add=$('#add'),
    $delete=$('#delete'),
    $table=$('#userlist'),
    $look=$('#look');
$look.prop('disabled',true);
$edit.prop('disabled',true);
$add.prop('disabled',false);
$delete.prop('disabled',true);
(function(document, window, $) {


  // Example Bootstrap Table Events
  // ------------------------------
  (function() {
    function delete_all(ids){
      ids.forEach((val,index)=>{
        delete val["user"];
        delete val["pwd"];
        delete val["username"];
        delete val["authority"];
        delete val["state"];
      })
      ids=JSON.stringify(ids);
      $.ajax({
        type:'POST',
        url:'user_delete.html?do=delete',
        data:ids,
        dataType:'JSON',
        success:function (){
          layer.msg('删除成功');

        },
        error:function (){
          layer.msg('删除失败');
        }
      })
    }
    function getSelections(){
      return $.map($table.bootstrapTable('getSelections'),function (row){
        return row;
      })
    }
    $add.on('click',function (){
      layer.open({
        type: 2,
        title: '增加用户',
        shadeClose: false,
        shade: 0.8,
        area: ['50%', '60%'],
        content: 'user_add.html',
        end:function (){
          $table.bootstrapTable('refresh');
        }
      });
    })
    $look.on('click',function (){
      var row=getSelections()[0];
      var Id=row.Id
      layer.open({
        type: 2,
        title: '查看用户信息',
        shadeClose: false,
        shade: 0.8,
        area: ['50%', '60%'],
        content: 'user_look.html?Id='+Id,
      });
    })
    $edit.on('click',function (){
      var row=getSelections()[0];
      var Id=row.Id;
      layer.open({
        type: 2,
        title: '修改用户信息',
        shadeClose: false,
        shade: 0.8,
        area: ['50%', '60%'],
        content: 'user_edit.html?Id=' + Id,
        end:function (){
          $table.bootstrapTable('refresh');
        }
      });
    })
    $delete.on('click',function (){
      var ids=getSelections();
      if (ids.length==0){
        layer.msg('请先选择至少一个用户');
      }else{
        layer.confirm('确定删除这'+ids.length+'个用户？',{
          btn:['是','否']
        },function (){
          delete_all(ids);
          $table.bootstrapTable('refresh');
        })
      }

    })
    $table.bootstrapTable({
      url: "user_json",
      dataType:'json',
      method:'post',
      search: true,
      pagination: true,
      clickToSelect: true,
      showRefresh: true,
      showToggle: true,
      showColumns: true,
      iconSize: 'outline',
      toolbar: '#exampleTableEventsToolbar',
      icons: {
        refresh: 'glyphicon-repeat',
        toggle: 'glyphicon-list-alt',
        columns: 'glyphicon-list'
      },
    });
    $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
      var bool=!(
        $table.bootstrapTable('getSelections').length &&
        $table.bootstrapTable('getSelections').length==1
      )
      $edit.prop('disabled',bool);
      $look.prop('disabled',bool);
      var bool2=!(
        $table.bootstrapTable('getSelections').length
      )
      $delete.prop('disabled',bool2)
    })
    // var $result = $('#examplebtTableEventsResult');
    //
    // $table.on('all.bs.table', function(e, name, args) {
    //     console.log('Event:', name, ', data:', args);
    //   })
    //   .on('click-row.bs.table', function(e, row, $element) {
    //     $result.text('Event: click-row.bs.table');
    //   })
    //   .on('dbl-click-row.bs.table', function(e, row, $element) {
    //     $result.text('Event: dbl-click-row.bs.table');
    //   })
    //   .on('sort.bs.table', function(e, name, order) {
    //     $result.text('Event: sort.bs.table');
    //   })
    //   .on('check.bs.table', function(e, row) {
    //     $result.text('Event: check.bs.table');
    //
    //   })
    //   .on('uncheck.bs.table', function(e, row) {
    //     $result.text('Event: uncheck.bs.table');
    //   })
    //   .on('check-all.bs.table', function(e) {
    //     $result.text('Event: check-all.bs.table');
    //   })
    //   .on('uncheck-all.bs.table', function(e) {
    //     $result.text('Event: uncheck-all.bs.table');
    //   })
    //   .on('load-success.bs.table', function(e, data) {
    //     $result.text('Event: load-success.bs.table');
    //   })
    //   .on('load-error.bs.table', function(e, status) {
    //     $result.text('Event: load-error.bs.table');
    //   })
    //   .on('column-switch.bs.table', function(e, field, checked) {
    //     $result.text('Event: column-switch.bs.table');
    //   })
    //   .on('page-change.bs.table', function(e, size, number) {
    //     $result.text('Event: page-change.bs.table');
    //   })
    //   .on('search.bs.table', function(e, text) {
    //     $result.text('Event: search.bs.table');
    //   });
  })();
})(document, window, jQuery);
