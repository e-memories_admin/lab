<?php
namespace app\home\controller;
use think\Controller;
use think\Cookie;
use think\Db;
/*
 * 权限代码
 * 4:系统管理员
 * 3：资产管理员
 * 2：实验室主任
 * 1：实验员
 * 0：授权用户
*/
class Index extends Controller
{

    public function index()  //内页主页
    {
        $cookie=new Cookie();
        $safecode=$cookie->get('safecode');
        $user_data=Db::name('user')->where('safecode',$safecode)->find();
        if(empty($user_data)){
            $this->error('请先登录','/index/index/login','',1);
        }
        $this->assign('user',$user_data);
        $this->assign('title','内页');
        return $this->fetch('home',[],['__PUBLIC__'=>'/public/static']);
    }
    public function home() //首页
    {
        return $this->fetch('index',[],['__PUBLIC__'=>'/public/static']);
    }
    public function loginout()  //退出登录
    {
        $cookie=new Cookie();
        $cookie->delete('safecode');
        $this->redirect('/index/index/login');
    }
}
