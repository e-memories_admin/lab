<?php
namespace app\home\controller;
use think\Controller;
use think\Cookie;
use think\Db;
/*
 * 权限代码
 * 4:系统管理员
 * 3：资产管理员
 * 2：实验室主任
 * 1：实验员
 * 0：授权用户
*/
class Worklog extends Controller
{
    /////////////私有类//////////////////
    function authid_to_name($authority)   //用户权限id转权限名字
    {
        switch ($authority){
            case 4:
                return '系统管理员';
                break;
            case 3:
                return '资产管理员';
                break;
            case 2:
                return '实验室主任';
                break;
            case 1:
                return '实验员';
                break;
            case 0:
                return '授权用户';
                break;
        }
    }
    function type_id_to_name($type)   //日志类型id转文本
    {
        switch ($type){
            case 1:
                return '开会';
                break;
            case 2:
                return '实验课';
                break;
            case 3:
                return '维修设备';
                break;
            case 4:
                return '出差';
                break;
            case 5:
                return '考试管理';
                break;
        }
    }
    function subtext($text, $length)    //字符串过长截取
    {
        if(mb_strlen($text, 'utf8') > $length)
            return mb_substr($text,0,$length,'utf8').'…';
        return $text;
    }
    function sexid_to_name($sex)   //用户权限id转权限名字
    {
        switch ($sex){
            case 1:
                return '男';
                break;
            case 0:
                return '女';
                break;
        }
    }
    function authority($array)  //权限管理
    {
        $cookie=new Cookie();
        $safecode=$cookie->get('safecode');
        $user_data=Db::name('user')->where('safecode',$safecode)->find();
        $authority_id=$user_data['authority'];
        if(!in_array($authority_id,$array)){
            $this->error('无权访问','../Index/home');
            exit();
        }
    }
    function get_user_data()    //获取个人用户信息
    {
        $cookie=new Cookie();
        $safecode=$cookie->get('safecode');
        $user_data=Db::name('user')->where('safecode',$safecode)->find();
        return $user_data;
    }
    function get_machine_name($machine_id) //根据设备id获取设备名称(资产id)
    {
        $machine=Db::name('machine')->where('asset_id',$machine_id)->find();
        return $machine['asset_name'];
    }
    function get_machine_model($machine_id) //根据设备id获取设备型号(资产id)
    {
        $machine=Db::name('machine')->where('asset_id',$machine_id)->find();
        return $machine['model'];
    }
    ////////////////////导出/////////////////
    public function export_work_diary()     //导出工作日志
    {
        $work = Db::name('work_diary');
        $user=$this->get_user_data();
        $uid=$user['uid'];
        $list = $work->where('uid',$uid)->select();
        foreach ($list as $index => $item) {
            if ($item['type']==1) {
                $list[$index]['type'] = '会议';
            }elseif ($item['type']==2){
                $list[$index]['type'] = '实验课';
            }elseif ($item['type']==3){
                $list[$index]['type'] = '维修设备';
            }elseif ($item['type']==4){
                $list[$index]['type'] = '出差';
            }elseif ($item['type']==5){
                $list[$index]['type'] = '考试管理';
            }
            $list[$index]['uid']='['.$uid.']'.$user['name']; //将用户名代替
        }
        $objPHPExcel = new \PHPExcel();
        // 设置sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // 设置列的宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        // 设置表头
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', '记录id');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', '用户名');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', '记录内容');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', '记录时间');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', '记录类别');
        //存取数据
        $num = 2;
        foreach ($list as $k => $v) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $num, $v['record_id']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $num, $v['uid']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $num, $v['content']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $num, $v['record_time']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $num, $v['type']);
            $num++;
        }
        // 文件名称
        $fileName = $user['name']."的工作日志" . date('Y-m-d', time()) . rand(1, 1000);
        $xlsName = iconv('utf-8', 'gb2312', $fileName);
        // 设置工作表名
        $objPHPExcel->getActiveSheet()->setTitle('sheet');
        //下载 excel5与excel2007
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        ob_end_clean();     // 清除缓冲区,避免乱码
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-execl;charset=UTF-8");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header("Content-Disposition:attachment;filename=" . $xlsName . ".xlsx");
        header("Content-Transfer-Encoding:binary");
        $objWriter->save("php://output");

    }
    public function export_repair_diary()     //导出维修日志
    {
        $work = Db::name('repair_diary');
        $user=$this->get_user_data();
        $uid=$user['uid'];
        $list = $work->where('recorder_id',$uid)->select();
        foreach ($list as $index => $item) {
            $list[$index]['machine_name']=$this->get_machine_name($item['machine_id']);
            $list[$index]['machine_model']=$this->get_machine_model($item['machine_id']);
            $list[$index]['recorder_id']='['.$uid.']'.$user['name']; //将用户名代替
        }
        $objPHPExcel = new \PHPExcel();
        // 设置sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // 设置列的宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        // 设置表头
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', '记录id');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', '设备编号');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', '设备名');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', '设备型号');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', '故障描述');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', '处理方法');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', '处理结果');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', '处理人');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', '记录时间');
        //存取数据
        $num = 2;
        foreach ($list as $k => $v) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $num, $v['record_id']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $num, $v['machine_id']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $num, $v['machine_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $num, $v['machine_model']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $num, $v['fault_description']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $num, $v['processing_method']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $num, $v['processing_result']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $num, $v['recorder_id']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $num, $v['record_time']);
            $num++;
        }
        // 文件名称
        $fileName = $user['name']."的维修日志" . date('Y-m-d', time()) . rand(1, 1000);
        $xlsName = iconv('utf-8', 'gb2312', $fileName);
        // 设置工作表名
        $objPHPExcel->getActiveSheet()->setTitle('sheet');
        //下载 excel5与excel2007
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        ob_end_clean();     // 清除缓冲区,避免乱码
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-execl;charset=UTF-8");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header("Content-Disposition:attachment;filename=" . $xlsName . ".xlsx");
        header("Content-Transfer-Encoding:binary");
        $objWriter->save("php://output");

    }

    ///////////////////////////工作日志////////////////////////////
    public function work()  //工作日志
    {
        $this->authority(['4','2']);
        $user=$this->get_user_data();
        $uid=$user['uid'];
        $work_data=Db::name('work_diary')->where('uid',$uid)->order('record_time','desc')->paginate(8);//tp5自带的分页，每页显示8个
        $this->assign('work_data',$work_data);
        return $this->fetch('work',[],['__PUBLIC__'=>'/public/static']);

    }
    public function add_work_diary()    //记录新的工作日志
    {
        $this->authority(['4','2']);
        $user=$this->get_user_data();
        if (input('post.')){
            $content=input('post.content');
            $type=input('post.type');
            $record_time=date("Y-m-d H:i:s",time());
            $uid=$user['uid'];
            try {
                Db::name('work_diary')->insert([
                    'uid'=>$uid,
                    'content'=>$content,
                    'record_time'=>$record_time,
                    'type'=>$type
                ]);
            }catch (\Exception $e){
                $this->assign('msg','数据库错误'.$e);
                return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
            }
            $this->assign('msg','日志上传成功');
            return $this->fetch('do_success',[],['__PUBLIC__'=>'/public/static']);
        }
        return $this->fetch('add_work_diary',[],['__PUBLIC__'=>'/public/static']);
    }
    public function work_json() //日志内容的接口，用于联想搜索
    {
        $this->authority(['4','2']);
        $user=$this->get_user_data();
        $uid=$user['uid'];
        $content=input('get.content');
        $datas=Db::name('work_diary')->where('uid',$uid)->where('content','like','%'.$content.'%')->limit(5)->select();
        $content=array();
        foreach ($datas as $data){
            $content[]=[
                'description'=>$this->type_id_to_name($data['type']),
                'content'=>$data['content']
            ];
        }
        $arr=[
            'message'=>"",
            'value'=>$content,
            'code'=>'200',
            'redirect'=>''
        ];
        return json($arr);
    }
    /////////////////////维修日志///////////////////////
    public function repair()    //维修日志列表
    {
        $this->authority(['4','2']);
        $user=$this->get_user_data();
        $uid=$user['uid'];
        $repair_data=Db::name('repair_diary')->where('recorder_id',$uid)->order('record_time','desc')->paginate(8);//tp5自带的分页，每页显示8个
        $this->assign('repair_data',$repair_data);
        return $this->fetch('repair',[],['__PUBLIC__'=>'/public/static']);
    }
    public function add_repair_diary()    //添加维修日志
    {
        $this->authority(['4','2']);
        $user=$this->get_user_data();
        if (input('post.')){
            $machine_id=input('post.machine_id');
            $fault_description=input('post.fault_description');
            $processing_method=input('post.processing_method');
            $processing_result=input('post.processing_result');
            $record_time=date("Y-m-d H:i:s",time());
            $recorder_id=$user['uid'];
            try {
                Db::name('repair_diary')->insert([
                    'machine_id'=>$machine_id,
                    'fault_description'=>$fault_description,
                    'processing_method'=>$processing_method,
                    'processing_result'=>$processing_result,
                    'record_time'=>$record_time,
                    'recorder_id'=>$recorder_id
                ]);
            }catch (\Exception $e){
                $this->assign('msg','数据库错误'.$e);
                return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
            }
            $this->assign('msg','日志上传成功');
            return $this->fetch('do_success',[],['__PUBLIC__'=>'/public/static']);
        }
        return $this->fetch('add_repair_diary',[],['__PUBLIC__'=>'/public/static']);
    }
    public function machine_data_json() //获取机器信息返回为json,用于联想搜索获取机器id
    {
        $arr=[
            'message'=>'',
            'value'=>Db::name('machine')->select(),
            'code'=>'200',
            'redirect'=>''
        ];
        return json($arr);
    }
    public function repair_json() //维修日志内容的接口，用于联想搜索
    {
        $this->authority(['4','2']);
        $user=$this->get_user_data();
        $uid=$user['uid'];
        $content=input('get.content');
        if(input('get.type')=='fault_description')
            $datas=Db::name('repair_diary')->where('recorder_id',$uid)->where('fault_description','like','%'.$content.'%')->limit(5)->select();
        if(input('get.type')=='processing_method')
            $datas=Db::name('repair_diary')->where('recorder_id',$uid)->where('processing_method','like','%'.$content.'%')->limit(5)->select();
        if(input('get.type')=='processing_result')
            $datas=Db::name('repair_diary')->where('recorder_id',$uid)->where('processing_result','like','%'.$content.'%')->limit(5)->select();
        $arr=[
            'message'=>"",
            'value'=>$datas,
            'code'=>'200',
            'redirect'=>''
        ];
        return json($arr);
    }
}
