<?php
namespace app\home\controller;
use think\Controller;
use think\Cookie;
use think\Db;
/*
 * 权限代码
 * 4:系统管理员
 * 3：资产管理员
 * 2：实验室主任
 * 1：实验员
 * 0：授权用户
*/
class User extends Controller
{
    function authid_to_name($authority)   //用户权限id转权限名字
    {
        switch ($authority){
            case 4:
                return '系统管理员';
                break;
            case 3:
                return '资产管理员';
                break;
            case 2:
                return '实验室主任';
                break;
            case 1:
                return '实验员';
                break;
            case 0:
                return '授权用户';
                break;
        }
    }
    function sexid_to_name($sex)   //用户权限id转权限名字
    {
        switch ($sex){
            case 1:
                return '男';
                break;
            case 0:
                return '女';
                break;
        }
    }
    function authority($array){
        $cookie=new Cookie();
        $safecode=$cookie->get('safecode');
        $user_data=Db::name('user')->where('safecode',$safecode)->find();
        $authority_id=$user_data['authority'];
        if(!in_array($authority_id,$array)){
            $this->error('无权访问','../Index/home');
            exit();
        }
    }
    public function user()  //用户管理 权限：4
    {
        $this->authority([4]);
        return $this->fetch('user',[],['__PUBLIC__'=>'/public/static']);
    }
    public function user_look()  //用户查看
    {
        $this->authority([4]);
        $Id=input('get.Id');
        $user_data=Db::name('user')->where('Id',$Id)->find();
        $this->assign('user',$user_data);
        return $this->fetch('user_look',[],['__PUBLIC__'=>'/public/static']);
    }
    public function user_add()  //用户增加
    {
        $this->authority([4]);
        if(input('post.')){
            $data=input('post.');
            if($data['uid']==''){
                $this->error('用户编号不能为空');
            }
            if($data['name']==''){
                $this->error('用户名不能为空');
            }
            if($data['pwd']==''){
                $this->error('密码不能为空');
            }
            if($data['auth_lab']==''){
                $this->error('实验室不能为空');
            }
            $uid=$data['uid'];
            $name=$data['name'];
            $pwd=$data['pwd'];
            $authority=$data['authority'];
            $auth_lab=$data['auth_lab'];
            $sex=$data['sex'];
            $user_data=Db::name('user')->where('uid','=',$uid)->find();
            if(!empty($user_data)){
                $this->assign('msg','编号存在！');
                return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
            }else{
                if(Db::name('user')->insert([
                    'uid'=>$uid,
                    'name'=>$name,
                    'pwd'=>$pwd,
                    'sex'=>$sex,
                    'authority'=>$authority,
                    'auth_lab'=>$auth_lab
                ])){
                    $this->assign('msg','操作成功');
                    return $this->fetch('do_success',[],['__PUBLIC__'=>'/public/static']);
                }else{
                    $this->assign('msg','操作失败');
                    return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
                }
            }
        }else{
            return $this->fetch('user_add',[],['__PUBLIC__'=>'/public/static']);
        }
    }
    public function user_edit()  //用户信息修改
    {
        $this->authority([4]);
        if(input('get.')){
            if(input('post.')){
                $data=input('post.');
                if($data['uid']==''){
                    $this->error('用户编号不能为空');
                }
                if($data['name']==''){
                    $this->error('用户名不能为空');
                }
                if($data['pwd']==''){
                    $this->error('密码不能为空');
                }
                if($data['auth_lab']==''){
                    $this->error('实验室不能为空');
                }
                $Id=input('get.Id');
                $uid=$data['uid'];
                $name=$data['name'];
                $pwd=$data['pwd'];
                $authority=$data['authority'];
                $auth_lab=$data['auth_lab'];
                $sex=$data['sex'];
                $map1=[
                    ['Id','<>',$Id],
                    ['uid','=',$uid]
                ];
                $user_data=Db::name('user')->where($map1)->find();
                if(!empty($user_data)){
                    $this->assign('msg','用编号存在！');
                    return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
                }else{
                    if(Db::name('user')->where('Id',$Id)->update([
                        'uid'=>$uid,
                        'name'=>$name,
                        'pwd'=>$pwd,
                        'sex'=>$sex,
                        'authority'=>$authority,
                        'auth_lab'=>$auth_lab
                    ])){
                        $this->assign('msg','操作成功');
                        return $this->fetch('do_success',[],['__PUBLIC__'=>'/public/static']);
                    }else{
                        $this->assign('msg','操作失败');
                        return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
                    }
                }

            }else{
                $user_data=Db::name('user')->where('Id',input('get.Id'))->find();
                $this->assign('user',$user_data);
                return $this->fetch('user_edit',[],['__PUBLIC__'=>'/public/static']);
            }

        }else{
            $this->error('非法访问');
        }

    }
    public function user_json()  //用户管理_json数据 权限：4
    {
        $this->authority([4]);
        $user_data=Db::name('user')->select();
        $arr=array();
        foreach ($user_data as $one_user){
            $arr1=array(
                'Id'=>$one_user['Id'],
                'uid'=>$one_user['uid'],
                'name'=>$one_user['name'],
                'sex'=>$this->sexid_to_name($one_user['sex']),
                'pwd'=>$one_user['pwd'],
                'authority'=>$this->authid_to_name($one_user['authority']),
                'auth_lab'=>$one_user['auth_lab']
            );
            $arr[]=$arr1;

        }
        return json($arr);
    }
    public function user_delete()   //删除用户
    {
        $this->authority([4]);
        $data=file_get_contents('php://input');
        $data=json_decode($data,true);
        $arr=array();
        foreach ($data as $one){
            $arr[]=$one['Id'];
        }
        if(Db::name('user')->delete($arr)) {
            return json(['msg' => 'success']);
        }else{
            $this->assign('msg','操作失败');
            return $this->fetch('do_error',[],['__PUBLIC__'=>'/public/static']);
        }
    }

}
