<?php
namespace app\index\controller;

use think\Controller;
use think\Cookie;
use think\Db;
class Index extends Controller
{
    function GetRandStr($len) //随机码生成函数
    {
        $chars = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k","l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v","w", "x", "y", "z","0", "1", "2","3", "4", "5", "6", "7", "8", "9");
        $charsLen = count($chars) - 1;
        shuffle($chars);
        $output = "";
        for ($i=0; $i<$len; $i++){
            $output .= $chars[mt_rand(0, $charsLen)];
        }
        return $output;
    }
    public function login() //登录页面
    {
        $db=Db::name('user');
        if(input('post.')){
            $uid=input('post.user');
            $pwd=input(('post.password'));
//            $map1=[
//                ['uid','=',$uid],
//                ['pwd','=',$pwd]
//            ];
//            $map2=[
//                ['username','=',$user],
//                ['pwd','=',$pwd]
//            ];
            $user_data=$db->where(['uid'=>$uid,'pwd'=>$pwd])->find();

            //可选择用户名或者账号进行登录
            if(!empty($user_data)){
                $safecode=$this->GetRandStr(20);
                Db::name('user')->where(['uid'=>$uid,'pwd'=>$pwd])->update(['safecode'=>$safecode]);
                $cookie=new Cookie();
                $cookie->set('safecode',$safecode,3600*12);
                $this->success('登陆成功','/home/Index/index','',1);
            }else{
                $this->error('账号名或者密码错误');
            }
        }
        //模板赋值
        $this->assign('title','登录');
        //模板输出
        return $this->fetch('login');

    }
}
