﻿

DROP TABLE IF EXISTS `lab_machine`;
CREATE TABLE `lab_machine` (
  `asset_id` varchar(255) NOT NULL COMMENT '资产编号',
  `inside_id` int(10) unsigned NOT NULL auto_increment COMMENT '内部编号',
  `asset_name` char(8) NOT NULL COMMENT '资产名称',
  `unit_price` float NOT NULL COMMENT '单价',
  `manager` varchar(255) NOT NULL COMMENT '管理人',
  `depositary` varchar(255) NOT NULL COMMENT '存放处',
  `model` varchar(255) NOT NULL COMMENT '型号',
  `standard` varchar(255) NOT NULL COMMENT '规格',
  `time` date NOT NULL COMMENT '购置时间',
  `status` varchar(255) default NULL,
  `comment` text NOT NULL COMMENT '备注',
  PRIMARY KEY  (`asset_id`),
  UNIQUE KEY `asset_number` (`asset_id`,`inside_id`),
  UNIQUE KEY `lab_machine_asset_number_uindex` (`asset_id`),
  UNIQUE KEY `lab_machine_inside_id_uindex` (`inside_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `lab_repertory`;
CREATE TABLE `lab_repertory` (
  `material_id` int(8) unsigned zerofill NOT NULL auto_increment COMMENT ' 耗材编号 ',
  `material_name` varchar(255) NOT NULL COMMENT ' 耗材名称 ',
  `material_model` varchar(255) NOT NULL COMMENT ' 型号规格 ',
  `num` int(11) NOT NULL COMMENT ' 数量 ',
  `comment` text NOT NULL COMMENT ' 备注 ',
  PRIMARY KEY  (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='耗材库存表';

DROP TABLE IF EXISTS `lab_user`;
CREATE TABLE `lab_user` (
  `Id` int(11) NOT NULL auto_increment,
  `uid` int(8) unsigned zerofill NOT NULL default '00000000' COMMENT ' 人员编号 ',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `pwd` varchar(255) NOT NULL COMMENT ' 密码 ',
  `sex` int(1) NOT NULL default '0',
  `authority` int(1) NOT NULL default '0' COMMENT ' 权限（1表示管理员，0表示普通用户）',
  `safecode` varchar(255) NOT NULL COMMENT ' 安全码 ',
  `auth_lab` varchar(255) default NULL COMMENT '管理实验室',
  PRIMARY KEY  (`Id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `lab_user` VALUES (0,00000001,'admin','123456',0,4,'49w7xo7e1yw012u6rdqw','实验室');

DROP TABLE IF EXISTS `lab_use`;
CREATE TABLE `lab_use` (
  `Id` int(11) NOT NULL auto_increment COMMENT '编号',
  `material_name` varchar(255) NOT NULL COMMENT ' 耗材名称 ',
  `material_id` int(8) unsigned zerofill NOT NULL COMMENT ' 耗材编号 ',
  `num` int(11) NOT NULL COMMENT ' 使用数量 ',
  `use_area` text NOT NULL COMMENT ' 用途 ',
  `user_id` int(8) unsigned zerofill NOT NULL default '00000000' COMMENT ' 使用人id ',
  `user` varchar(255) NOT NULL COMMENT ' 使用人 ',
  `receive_time` datetime NOT NULL COMMENT ' 领取时间 ',
  `approval_status` int(2) NOT NULL default '0' COMMENT ' 审批状态（0待审，1通过，2不通过，3审核中）',
  `material_model` varchar(255) default NULL,
  PRIMARY KEY  (`Id`),
  KEY `user_id` (`user_id`),
  KEY `lab_use_lab_repertory_material_id_fk` (`material_id`),
  CONSTRAINT `lab_use_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `lab_user` (`uid`),
  CONSTRAINT `lab_use_lab_repertory_material_id_fk` FOREIGN KEY (`material_id`) REFERENCES `lab_repertory` (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='耗材使用表';


DROP TABLE IF EXISTS `lab_repair_diary`;
CREATE TABLE `lab_repair_diary` (
  `record_id` int(10) NOT NULL auto_increment COMMENT '记录号',
  `machine_id` varchar(255) NOT NULL COMMENT '设备号',
  `fault_description` varchar(255) NOT NULL COMMENT '故障描述',
  `processing_method` varchar(255) NOT NULL COMMENT '处理方式',
  `processing_result` varchar(255) NOT NULL COMMENT '处理结果',
  `recorder_id` int(8) unsigned zerofill NOT NULL COMMENT '（用户编号）记录人',
  `record_time` datetime default NULL,
  PRIMARY KEY  (`record_id`),
  UNIQUE KEY `lab_repair_diary_record_id_uindex` (`record_id`),
  KEY `lab_repair_diary___uid` (`recorder_id`),
  CONSTRAINT `lab_repair_diary___uid` FOREIGN KEY (`recorder_id`) REFERENCES `lab_user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `lab_apply`;
CREATE TABLE `lab_apply` (
  `apply_id` int(11) NOT NULL auto_increment COMMENT '申请编号',
  `material_name` varchar(255) NOT NULL COMMENT ' 耗材名称 ',
  `material_model` varchar(255) NOT NULL COMMENT ' 型号规格 ',
  `material_num` int(10) NOT NULL COMMENT ' 数量 ',
  `applicant_id` int(8) unsigned zerofill NOT NULL default '00000000' COMMENT ' 申请人id ',
  `applicant` varchar(255) NOT NULL COMMENT ' 申请人 ',
  `apply_time` datetime NOT NULL COMMENT ' 申请时间 ',
  `approval_status` int(2) NOT NULL default '0' COMMENT ' 审批状态（0待审，1通过，2不通过，3审核中）',
  PRIMARY KEY  (`apply_id`),
  KEY `applicant_id` (`applicant_id`),
  CONSTRAINT `lab_apply_ibfk_1` FOREIGN KEY (`applicant_id`) REFERENCES `lab_user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='耗材购置申请表';


DROP TABLE IF EXISTS `lab_purchase`;
CREATE TABLE `lab_purchase` (
  `Id` int(11) NOT NULL auto_increment COMMENT '编号',
  `material_name` varchar(255) NOT NULL COMMENT ' 耗材名称 ',
  `material_model` varchar(255) NOT NULL COMMENT ' 型号规格 ',
  `material_id` int(8) unsigned zerofill NOT NULL COMMENT ' 耗材编号 ',
  `material_num` int(10) NOT NULL COMMENT ' 数量 ',
  `get_num` int(10) default NULL COMMENT '实际采购数量',
  `material_unit_price` double(22) NOT NULL default '0' COMMENT ' 材料单价 ',
  `purchaser_id` int(10) unsigned default '0' COMMENT ' 购买人id ',
  `purchaser` varchar(255) default NULL COMMENT ' 采购人 ',
  `purchase_time` datetime NOT NULL COMMENT ' 采购时间 ',
  `comment` text COMMENT ' 备注 ',
  `apply_id` int(10) default NULL,
  `status` int(11) default '0' COMMENT '状态',
  PRIMARY KEY  (`Id`),
  KEY `lab_purchase_lab_repertory_material_id_fk` (`material_id`),
  KEY `lab_purchase_lab_apply_apply_id_fk` (`apply_id`),
  CONSTRAINT `lab_purchase_lab_apply_apply_id_fk` FOREIGN KEY (`apply_id`) REFERENCES `lab_apply` (`apply_id`),
  CONSTRAINT `lab_purchase_lab_repertory_material_id_fk` FOREIGN KEY (`material_id`) REFERENCES `lab_repertory` (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='耗材购置表';


DROP TABLE IF EXISTS `lab_work_diary`;
CREATE TABLE `lab_work_diary` (
  `record_id` int(10) NOT NULL auto_increment COMMENT '记录号',
  `uid` int(8) unsigned zerofill NOT NULL COMMENT '用户编号',
  `content` text NOT NULL COMMENT '内容',
  `record_time` datetime default NULL,
  `type` int(1) NOT NULL COMMENT '1表示会议性质，2表示实验课性质，3表示维修设备，4表示出差，5表示考试管理',
  PRIMARY KEY  (`record_id`),
  UNIQUE KEY `lab_work_diary_record_id_uindex` (`record_id`),
  KEY `lab_work_diary_to_uid` (`uid`),
  CONSTRAINT `lab_work_diary_to_uid` FOREIGN KEY (`uid`) REFERENCES `lab_user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
